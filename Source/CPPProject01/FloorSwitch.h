// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloorSwitch.generated.h"

UCLASS()
class CPPPROJECT01_API AFloorSwitch : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFloorSwitch();

	/** Overlap volume for functionally to be triggered*/
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Floor Switch")
	class UBoxComponent* TriggerBox;

	/** Switch for the character to step on*/
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category= "Floor Switch")
	class UStaticMeshComponent* FloorSwitchMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category= "Floor Switch")
	class UStaticMeshComponent* DoorMesh;

	/** Initial location for Door*/
	UPROPERTY(BlueprintReadWrite, Category = "Floor Switch")
	FVector InitialDoorLocation;

	/** Initial location for Floor Switch */
	UPROPERTY(BlueprintReadWrite, Category= "Floor Switch")
	FVector InitialSwitchLocation;

	UPROPERTY(EditAnywhere, Category = "Floor Switch")
	float SwitchTime;
	
	FTimerHandle SwitchHandle;

	bool bCharacterOnSwitch;
	

	
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
		const FHitResult &SweepResult);

	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintImplementableEvent, Category ="Floor Switch")
	void RaiseDoor();

	UFUNCTION(BlueprintImplementableEvent, Category ="Floor Switch")
	void LowerDoor();

	UFUNCTION(BlueprintImplementableEvent, Category ="Floor Switch")
	void RaiseFloorSwitch();

	UFUNCTION(BlueprintImplementableEvent, Category ="Floor Switch")
	void LowerFloorSwitch();

	UFUNCTION(BlueprintCallable, Category= "Floor Switch")
	void UpdateDoorLocation(float Z);

	UFUNCTION(BlueprintCallable, Category = "Floor Switch")
	void UpdateFloorSwitch(float Z);

	void CloseDoor();
};


