// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CPPProject01GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API ACPPProject01GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
