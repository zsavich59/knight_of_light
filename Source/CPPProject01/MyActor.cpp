// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActor.h"

// Sets default values
AMyActor::AMyActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bShowRunningTime = false;
	InitialOffset = FVector(0.0f);
	sinCount = 0.f;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	InitialOffset = GetActorLocation();
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	RunningTime += DeltaTime;
	sinCount = FMath::Sin(RunningTime);
	
	FVector NewLocation = GetActorLocation();
	float NewLocationZ = NewLocation.Z;
	
	NewLocation.Z = NewLocationZ + sinCount;

	SetActorLocation(NewLocation);

	if(bShowRunningTime)
	{
		UE_LOG(LogTemp, Warning, TEXT("Running Time: %f"), RunningTime);
	}
	
	if(bShowSinCount)
	{
		UE_LOG(LogTemp, Warning, TEXT("Sin: %f"), sinCount);
	}

	if(bShowZ)
	{
		UE_LOG(LogTemp, Warning, TEXT("Z: %f"), NewLocationZ);
	}
}

