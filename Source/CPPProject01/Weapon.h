// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Weapon.generated.h"

UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	EWS_Pickup		UMETA(DisplayName = "Pickup"),
	EWS_Equipped	UMETA(DisplayName = "Equipped"),
	
	EWS_MAX		UMETA(DisplayName = "DefaultMAX")
};

/**
 * 
 */
UCLASS()
class CPPPROJECT01_API AWeapon : public AItem
{
	GENERATED_BODY()
	
public:
	/** Default Constructor for AWeapon */
	AWeapon();

	UPROPERTY(EditDefaultsOnly, Category = "SavedData")
	FString WeaponName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	bool bWeaponParticles;
	
	/** Base Skeletal Mesh Component */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item")
	class USkeletalMeshComponent* SkeletalComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	class USoundCue* OnEquipSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	USoundCue* SwingSound;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item")
	EWeaponState WeaponState;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item | Combat")
	class UBoxComponent* CombatCollision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Combat")
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Combat")
	TSubclassOf<UDamageType> DamageTypeClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item | Combat")
	AController* WeaponInstiator;
	
protected:

	virtual void BeginPlay() override;
	
public:
	
	/** Overlap events Begin & End */
	virtual void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	virtual void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	UFUNCTION()
	void CombatOnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void CombatOnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
	
	/** Equip Weapon to Character */
	void Equip(class AMainCharacter* Character);

	UFUNCTION(BlueprintCallable)
	void ActivateCollision();
	UFUNCTION(BlueprintCallable)
	void DeactivateCollision();

	FORCEINLINE void SetWeaponState(EWeaponState State) {WeaponState = State;}
	FORCEINLINE EWeaponState GetWeaponState() const {return WeaponState;}
	FORCEINLINE void SetInstigator(AController* Inst) {WeaponInstiator = Inst;}
};
