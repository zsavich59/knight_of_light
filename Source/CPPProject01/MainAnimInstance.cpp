// Fill out your copyright notice in the Description page of Project Settings.


#include "MainAnimInstance.h"

#include "MainCharacter.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Kismet/GameplayStatics.h"

void UMainAnimInstance::NativeInitializeAnimation()
{
	if(Pawn == nullptr)
	{
		Pawn = TryGetPawnOwner();
		if(Pawn)
		{
			Character = Cast<AMainCharacter>(Pawn);
		}
	}
}
void UMainAnimInstance::UpdateAnimationProperties()
{
	if(Pawn==nullptr)
	{
		Pawn = TryGetPawnOwner();
	}

	if(Pawn)
	{
		if(Character == nullptr)
		{
			Character = Cast<AMainCharacter>(Pawn); 
		}
		
		FVector Speed = Pawn->GetVelocity();
		FVector LateralSpeed = FVector(Speed.X, Speed.Y, 0.f);
		MovementSpeed = LateralSpeed.Size();

		bIsInAir = Pawn->GetMovementComponent()->IsFalling();

		
	}
}
