// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyActor.generated.h"

UCLASS()
class CPPPROJECT01_API AMyActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActor();
	UPROPERTY(VisibleAnywhere, Category = "Mesh")
	UStaticMeshComponent* StaticMesh;
	
	float RunningTime;
	float sinCount;
	
	UPROPERTY(EditInstanceOnly, Category = "Debug Information")
	bool bShowRunningTime;

	UPROPERTY(EditInstanceOnly, Category = "Debug Information")
	bool bShowSinCount;

	UPROPERTY(EditInstanceOnly, Category = "Debug Information")
	bool bShowZ;

	FVector InitialOffset;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
