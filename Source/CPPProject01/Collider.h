// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Collider.generated.h"

UCLASS()
class CPPPROJECT01_API ACollider : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACollider();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class USphereComponent* Sphere;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UColliderMovementComponent* OurMovementComponent;

	virtual UPawnMovementComponent* GetMovementComponent() const override;
	
	// Get & Set for StaticMeshComponent
	FORCEINLINE UStaticMeshComponent* GetMeshComponent() { return StaticMesh; }
	FORCEINLINE void SetMeshComponent(UStaticMeshComponent* _StaticMesh) {StaticMesh = _StaticMesh;}

	// Get & Set for Sphere
	FORCEINLINE USphereComponent* GetSphere() { return Sphere; }
	FORCEINLINE void SetSphere(USphereComponent* _Sphere) {Sphere = _Sphere;}

	// Get & Set for Camera
	FORCEINLINE UCameraComponent* GetCamera() { return Camera; }
	FORCEINLINE void SetCamera(UCameraComponent* _Camera) {Camera = _Camera;}

	// Get & Set for SpringArm
	FORCEINLINE USpringArmComponent* GetSpringArm() { return SpringArm; }
	FORCEINLINE void SetSpringArm(USpringArmComponent* _SpringArm) {SpringArm = _SpringArm;}
	
private:
	void MoveForward(float Value);
	void MoveRight(float Value);
	void CameraPitch(float AxisValue);
	void CameraYaw(float AxisValue);

	FVector2D CameraInput;
};
