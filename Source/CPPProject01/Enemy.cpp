// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"

#include "AIController.h"
#include "MainCharacter.h"
#include "MainPlayerController.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Sound/SoundCue.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AgroSphere = CreateDefaultSubobject<USphereComponent>(TEXT("AgroSphere"));
	AgroSphere->SetupAttachment(GetRootComponent());
	AgroSphere->InitSphereRadius(600.f);
	
	CombatSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CombatSphere"));
	CombatSphere->SetupAttachment(GetRootComponent());
	CombatSphere->InitSphereRadius(75.f);

	CombatCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CombatCollisionBox"));
	CombatCollisionBox->SetupAttachment(GetMesh(),"EnemySocket");
	CombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision );
	CombatCollisionBox->SetCollisionObjectType(ECC_WorldDynamic);
	CombatCollisionBox->SetCollisionResponseToAllChannels(ECR_Ignore);
	CombatCollisionBox->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	
	Health = 75.f;
	MaxHealth = 100.f;
	Damage = 15.f;

	AttackMinTime = 0.5f;
	AttackMaxTime = 3.5f;

	DeathDelay = 3.f;
	
	bHasValidTarget = false;

	SetEnemyMovementStatus(EEnemyMovementStatus::EMS_Idle);
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	AIController = Cast<AAIController>(GetController());

	AgroSphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::AgroSphereOnBeginOverlap);
	AgroSphere->OnComponentEndOverlap.AddDynamic(this, &AEnemy::AgroSphereOnEndOverlap);
	
	CombatSphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::CombatSphereOnBeginOverlap);
	CombatSphere->OnComponentEndOverlap.AddDynamic(this, &AEnemy::CombatSphereOnEndOverlap);

	CombatCollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::CombatOnBeginOverlap);
	CombatCollisionBox->OnComponentEndOverlap.AddDynamic(this, &AEnemy::AEnemy::CombatOnEndOverlap);

	bIsOverlappedCollision = false;
	bIsAttacking = false;

	GetMesh()->SetCollisionResponseToChannel(ECC_Camera,ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera,ECR_Ignore);
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemy::AgroSphereOnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor && Alive())
	{
		AMainCharacter* Character = Cast<AMainCharacter>(OtherActor);
		if(Character)
		{
			MoveToTarget(Character);
			
		}
	}
}

void AEnemy::AgroSphereOnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if(OtherActor)
	{
		AMainCharacter* Character = Cast<AMainCharacter>(OtherActor);
		if(Character)
		{
			bHasValidTarget = false;
			EnemyMovementStatus = EEnemyMovementStatus::EMS_Idle;
			if(Character->CombatTarget == this)
            {
            	Character->SetCombatTarget(nullptr);
            }
			Character->SetHasCombatTarget(false);
			Character->UpdateCombatTarget();
			if(AIController)
            {
            	AIController->StopMovement();
            }
		}
	}
}

void AEnemy::CombatSphereOnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor && Alive())
	{
		AMainCharacter* Character = Cast<AMainCharacter>(OtherActor);
		if(Character)
		{
			Character->SetCombatTarget(this);
			Character->SetHasCombatTarget(true);
			Character->UpdateCombatTarget();
			
			CombatTarget = Character;
			bIsOverlappedCollision = true;
			bHasValidTarget = true;
			
			float AttackTime = FMath::RandRange(AttackMinTime, AttackMaxTime);
			GetWorldTimerManager().SetTimer(AttackTimer, this, &AEnemy::Attack, AttackTime);
		}
	}
}

void AEnemy::CombatSphereOnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if(OtherActor && OtherComp)
	{
		AMainCharacter* Character = Cast<AMainCharacter>(OtherActor);
		if(Character)
		{
			bIsOverlappedCollision = false;
			if(EnemyMovementStatus != EEnemyMovementStatus::EMS_Dead)
			{
				MoveToTarget(Character);
				CombatTarget = nullptr;
			}
			if(Character->CombatTarget == this)
			{
				Character->SetCombatTarget(nullptr);
				Character->bHasCombatTarget = false;
				Character->UpdateCombatTarget();
			}
			if(Character->MainPlayerController)
			{
				USkeletalMeshComponent* CharacterMesh = Cast<USkeletalMeshComponent>(OtherComp);
				if(CharacterMesh) Character->MainPlayerController->RemoveEnemyHealthBar();
			}
			
			GetWorldTimerManager().ClearTimer(AttackTimer);
		}
	}
}

float AEnemy::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	if(Health - DamageAmount <= 0.f)
	{
		Health = 0;
		Die(DamageCauser);
	}
	else
	{
		Health -= DamageAmount;
	}
	
	return DamageAmount;
}

void AEnemy::CombatOnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                  UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor && Alive() )
	{
		AMainCharacter* Character = Cast<AMainCharacter>(OtherActor);
		if(Character)
		{
			if(Character->HitParticles)
			{
				const USkeletalMeshSocket* TipSocket = GetMesh()->GetSocketByName("TopSocket");
				if(TipSocket)
				{
					FVector TipSocketLocation = TipSocket->GetSocketLocation(GetMesh());
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Character->HitParticles, TipSocketLocation);
				}
			}
			if(Character->HitSound)
			{
				UGameplayStatics::PlaySound2D(this, Character->HitSound);
			}
			if(DamageTypeClass)
			{
				UGameplayStatics::ApplyDamage(Character, Damage, AIController, this, DamageTypeClass);
			}
		}
	}
}

void AEnemy::CombatOnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	
}

void AEnemy::MoveToTarget(AMainCharacter* Target)
{
	SetEnemyMovementStatus(EEnemyMovementStatus::EMS_MoveToTarget);
	
	if(AIController)
	{
		FAIMoveRequest MoveRequest;
		MoveRequest.SetGoalActor(Target);
		MoveRequest.SetAcceptanceRadius(25.0f);

		FNavPathSharedPtr NavPath;

		AIController->MoveTo(MoveRequest, &NavPath);

		
		/*
		 * Debug Path Points that responsible for creating way to target
		 * 
		TArray<FNavPathPoint> PathPoints = NavPath->GetPathPoints(); // OR auto PathPints = ...
		for(auto Point : PathPoints)
		{
			FVector Location = Point.Location;
			UKismetSystemLibrary::DrawDebugSphere(this, Location, 25.f,12,
				FLinearColor::Blue,25.f,2.f);
		}
		*/
	}
}

void AEnemy::ActivateCollision()
{
	CombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	
	if(SwingSound)
	{
		UGameplayStatics::PlaySound2D(this, SwingSound);
	}
}

void AEnemy::DeactivateCollision()
{
	CombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AEnemy::Attack()
{
	if(Alive() && bHasValidTarget)
	{
		if(AIController)
		{
			AIController->StopMovement();
			SetEnemyMovementStatus(EEnemyMovementStatus::EMS_Attacking);
		}
		if(!bIsAttacking)
		{
			bIsAttacking = true;
			UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
			if(AnimInstance && CombatMontage)
			{
				int32 Selector = FMath::RandRange(0,1);
				switch (Selector)
				{
				case 0:
					AnimInstance->Montage_Play(CombatMontage, 1.35f);
					AnimInstance->Montage_JumpToSection("Attack_A", CombatMontage);
					break;
				case 1:
					AnimInstance->Montage_Play(CombatMontage, 1.35f);
					AnimInstance->Montage_JumpToSection("Attack_B", CombatMontage);
					break;
				default:
					break;
				}
			}
		}
	}
}

void AEnemy::AttackEnd()
{
	bIsAttacking = false;
	if(bIsOverlappedCollision)
	{
		float AttackTime = FMath::RandRange(AttackMinTime, AttackMaxTime);
		GetWorldTimerManager().SetTimer(AttackTimer, this, &AEnemy::Attack, AttackTime);
	}
}

void AEnemy::Die(AActor* Causer)
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if(AnimInstance && CombatMontage)
	{
		AnimInstance->Montage_Play(CombatMontage, 1.0f);
		AnimInstance->Montage_JumpToSection(FName("Death"));
	}
	SetEnemyMovementStatus(EEnemyMovementStatus::EMS_Dead);
	AMainCharacter* Character = Cast<AMainCharacter>(Causer);
	if(Character)
	{
		Character->UpdateCombatTarget();
	}

	/** Set No Collision for Enemy when it's died*/
	CombatSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	AgroSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AEnemy::DeathEnd()
{
	GetMesh()->bPauseAnims = true;
	GetMesh()->bNoSkeletonUpdate = true;
	GetWorldTimerManager().SetTimer(DeathTimer, this, &AEnemy::Disappear, DeathDelay);
}

bool AEnemy::Alive()
{
	return GetEnemyMovementStatus() != EEnemyMovementStatus::EMS_Dead;
}

void AEnemy::Disappear()
{
	Destroy();
}

